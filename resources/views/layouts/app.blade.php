<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/vue.min.js') }}"></script>
    <script type="text/javascript" src='https://maps.google.com/maps/api/js?key={{ env('GOOGLE_MAP_API_KEY') }}&sensor=false&libraries=places'></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" integrity="sha256-UzFD2WYH2U1dQpKDjjZK72VtPeWP50NoJjd26rnAdUI=" crossorigin="anonymous" />
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Code+Pro" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/responsive.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/stripe-elements.css') }}" data-rel-css="" />
    <link rel="stylesheet" href="{{ asset('css/flatpickr.min.css') }}">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
    <div>
        <header>
            <nav class="navbar navbar-expand-md navbar-light">
                <div class="container">
                    <a class="navbar-brand" href="{{ url('/') }}" style="color: white">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <!-- Left Side Of Navbar -->
                        <ul class="navbar-nav mr-auto">

                        </ul>

                        <!-- Right Side Of Navbar -->
                        <ul class="navbar-nav ml-auto">
                            <!-- Menu Links -->
                            <li class="nav-item">
                                <a class="nav-link" href="javascript:void(0)">Spedisci un pacco</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="javascript:void(0)">Traccia un pacco</a>
                            </li>
                            @guest
                                @if (Route::has('register'))
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                    </li>
                                @endif
                                <li class="nav-item">
                                    <a class="login btn btn-success" href="{{ route('login') }}">Login</a>
                                </li>
                            @else
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <main>
            @yield('content')
        </main>

        <div class="footer">
            <div class="container">
                <div class="footer-menu">
                    <div class="row">
                        <div class="col-md-6 col-lg-3 col-sm-12">
                            <ul>
                                <li><a href="javascript:void(0)">Home</a></li>
                                <li><a href="javascript:void(0)">Spedisci un pacco</a></li>
                                <li><a href="javascript:void(0)">Traccia un pacco</a></li>
                                <li><a href="javascript:void(0)">Aiuto</a></li>
                                <li><a href="javascript:void(0)">Preferenze</a></li>
                                <li><a href="javascript:void(0)">Ricerca nel sito</a></li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-lg-3 col-sm-12">
                            <ul>
                                <li><a href="javascript:void(0)">Chi siamo</a></li>
                                <li><a href="javascript:void(0)">Spedizioni Internazionali</a></li>
                                <li><a href="javascript:void(0)">Spedizione Aziende</a></li>
                                <li><a href="javascript:void(0)">Spedizione Aziende</a></li>
                                <li><a href="javascript:void(0)">Il nostro blog</a></li>
                                <li><a href="javascript:void(0)">Termini e condizioni</a></li>
                            </ul>
                        </div>
                        <div class="col-md-12 col-lg-6 col-sm-12">
                            <ul>
                                <li><a href="javascript:void(0)">Politica Uso Accettabile</a></li>
                                <li><a href="javascript:void(0)">Politica di privacy</a></li>
                                <li><a href="javascript:void(0)">Politica di utilizzo dei cookie</a></li>
                                <li><a href="javascript:void(0)">Mappa del sito</a></li>
                            </ul>
                            <ul class="pay-list">
                                <li>
                                    <a href="#" title="">
                                        <img src="https://grandetest.com/theme/techno-html/images/logos/ft-01.png" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="">
                                        <img src="https://grandetest.com/theme/techno-html/images/logos/ft-02.png" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="">
                                        <img src="https://grandetest.com/theme/techno-html/images/logos/ft-03.png" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="">
                                        <img src="https://grandetest.com/theme/techno-html/images/logos/ft-04.png" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="#" title="">
                                        <img src="https://grandetest.com/theme/techno-html/images/logos/ft-05.png" alt="">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                {{--<div class="col-sm-12 text-center">
                    @php $pages = \App\Page::where('status', 1)->get(); @endphp
                    @foreach($pages as $page)
                        <a style="margin: 0 15px" href="{{ route('page.show', $page->slug) }}">
                            {{ $page->title }}
                        </a>
                    @endforeach
                </div>--}}
            </div>
        </div>
        <footer>
            <div class="container">
                <div class="footer-bottom">
                    <div class="row">
                        <div class="col-lg-11 col-md-11 col-sm-12">
                            <div class="text-left">
                                <h6>© 2019 TiSpedisco - A Tran Service Company - VAT IT12456789</h6>
                                <p>Tutti i testi e la grafica presenti nel sito sono soggetti alle norme vigenti sul diritto d autore.</p>
                            </div>
                        </div>
                        <div class="col-lg-1 col-md-1 col-sm-12">
                            <div class="text-right">
                                <p>IT / EN</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script>
        // Example starter JavaScript for disabling form submissions if there are invalid fields
        (function() {
            'use strict';
            window.addEventListener('load', function() {
                // Fetch all the forms we want to apply custom Bootstrap validation styles to
                var forms = document.getElementsByClassName('needs-validation');
                // Loop over them and prevent submission
                var validation = Array.prototype.filter.call(forms, function(form) {
                    form.addEventListener('submit', function(event) {
                        if (form.checkValidity() === false) {
                            event.preventDefault();
                            event.stopPropagation();
                        }
                        form.classList.add('was-validated');
                    }, false);
                });
            }, false);
        })();
    </script>
    @yield('script')

</body>
</html>
