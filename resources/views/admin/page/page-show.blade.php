@extends('admin.layouts.app')

@section('title')
    Page show
@endsection

@section('heading')
    Page show
@endsection

@section('breadcrumb')
    Page show
@endsection

@section('content')
    <!-- Row created callback -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="card-title">{{ $page->title }}</h4>
                        </div>
                        <div class="col-md-2 text-right">
                            <a href="{{ route('pages.index') }}" class="btn btn-info"><i class="mdi mdi-step-backward"></i> Back to list</a>
                        </div>
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>
                    {!! html_entity_decode($page->content) !!}
                </div>
            </div>
        </div>
    </div>
@endsection
