@extends('admin.layouts.app')

@section('title')
    Role create
@endsection

@section('heading')
    Role @if(isset($edit)) edit @else create @endif
@endsection

@section('breadcrumb')
    Role @if(isset($edit)) edit @else create @endif
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! html_entity_decode(Session::get('success')) !!}
        </div>
    @endif
    <!-- Row created callback -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="card-title">@if(isset($edit)) Edit @else Create @endif role</h4>
                        </div>
                        <div class="col-md-2 text-right">
                            <a href="{{ route('roles.index') }}" class="btn btn-info"><i class="mdi mdi-step-backward"></i> Back to list</a>
                        </div>
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>
                    <form method="post" action="@if(isset($edit)) {{ route('roles.update', $role->id) }} @else {{ route('roles.store') }} @endif" novalidate enctype="multipart/form-data">
                        @csrf
                        @if(isset($edit))
                            @method('PUT')
                        @endif
                        <div class="row">
                            <div class="col-lg-12 col-md-12">

                                <div class="form-group">
                                    <h5>Role title<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="title" value="@if(isset($edit)){{ $role->title }}@else{{ old('title') }}@endif" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Role title" required data-validation-required-message="Please provide a role title">
                                        @if ($errors->has('title'))
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('title') }}</strong>
                                                </span>
                                        @endif
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-12 col-md-12">
                                <hr>
                                <div class="text-xs-right">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <button type="reset" class="btn btn-inverse">Reset</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
