@extends('admin.layouts.app')

@section('title')
    Type create
@endsection

@section('heading')
    Type @if(isset($edit)) edit @else create @endif
@endsection

@section('breadcrumb')
    Type @if(isset($edit)) edit @else create @endif
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! html_entity_decode(Session::get('success')) !!}
        </div>
    @endif
    <!-- Row created callback -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="card-title">@if(isset($edit)) Edit @else Create @endif type</h4>
                        </div>
                        <div class="col-md-2 text-right">
                            <a href="{{ route('types.index') }}" class="btn btn-info"><i class="mdi mdi-step-backward"></i> Back to list</a>
                        </div>
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>
                    <form method="post" action="@if(isset($edit)) {{ route('types.update', $type->id) }} @else {{ route('types.store') }} @endif" novalidate enctype="multipart/form-data">
                        @csrf
                        @if(isset($edit))
                            @method('PUT')
                        @endif
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <h5>Title<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="title" value="@if(isset($edit)){{ $type->title }}@else{{ old('title') }}@endif" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                        @error ('title')
                                        <div class="invalid-tooltip">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12">
                                <hr>
                                <div class="text-xs-right">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <button type="reset" class="btn btn-inverse">Reset</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
