<?php

namespace App\Http\Controllers\User;

use App\User;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('verified')->except('users', 'updateUsers', 'show', 'edit');
        $this->middleware('auth:admin')->only('users', 'updateUsers', 'show', 'edit');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // Get all users
        $user = User::find(auth()->user()->id);
        $addresses = json_encode($user->addresses);

        return view('user.dashboard', compact('user', 'addresses'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function users()
    {
        // Get all users
        $clients = User::all()->sortByDesc('id');

        return view('admin.user.users', compact('clients'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        // Find the model
        $user = User::find($user->id);

        return view('admin.user.user-show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        // Find the model
        $user = User::find($user->id);

        return view('admin.user.user-edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        // Validate form data
        $rules = array(
            'name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'city' => 'required|string|max:255',
            'country' => 'required|string|max:255',
            'state' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
            'zipCode' => 'required|integer',
        );
        $validator = Validator::make ( $request->all(), $rules);

        if ($validator->fails()){
            return response()->json(array('errors'=> $validator->getMessageBag()->toarray()));
        }

        // Find the model
        $user = User::find($user->id);
        $user->name = $request->name;
        $user->address = $request->address;
        $user->city = $request->city;
        $user->country = $request->country;
        $user->state = $request->state;
        $user->zipCode = $request->zipCode;
        $user->phone = $request->phone;
        $user->save();

        return response()->json($user);
    }

    /**
     * Update client password.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request)
    {
        // Validate form data
        $rules = array(
            'password' => 'required|string|min:8',
            'new_password' => 'required|string|min:8|confirmed',
        );
        $validator = Validator::make ( $request->all(), $rules);

        if ($validator->fails()){
            return response()->json(array('errors' => $validator->getMessageBag()->toarray()));
        }

        // Get the client
        $client = User::find(auth()->user()->id);
        if (!Hash::check($request->password, $client->password)){
            // Return json response
            return response()->json(['errors' => ['error' => 'Your old password did not match!']]);
        }

        // Upadate password
        $client->password = Hash::make($request->new_password);
        $client->save();

        // Return json response
        return response()->json();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User  $user)
    {
        // Remove the specified resource from storage
        User::destroy($user->id);

        return redirect()->back()->with('success', '<strong>Client</strong> deleted successfully.');
    }

    /**
     * Upload client image.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function uploadImage(Request $request)
    {
        // Validate form data
        $rules = array(
            'avatar' => 'required|image',
        );
        $validator = Validator::make ( $request->all(), $rules);

        if ($validator->fails()){
            return response()->json(array('errors' => $validator->getMessageBag()->toarray()));
        }

        // Get the client
        $client = User::find(auth()->user()->id);

        // Upload image
        if ($request->hasFile('avatar')) {
            //get filename with extension
            $filenamewithextension = $request->file('avatar')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('avatar')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;

            //Upload File
            $request->file('avatar')->storeAs('public/images/profile_images/thumbnail', $filenametostore);

            //Resize image here
            $thumbnailpath = public_path('storage/images/profile_images/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(100, 100, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);

            // Delete previous image from directory
            if ($client->avatar) {
                Storage::delete('public/images/profile_images/thumbnail/'.$client->avatar);
            }
        }

        $client->avatar = $filenametostore;
        $client->save();

        // Return json response
        return response()->json($filenametostore);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updateUsers(Request $request, User $user)
    {
        // Validate form data
        $request->validate(array(
            'name' => 'required|string|max:255',
            'avatar' => 'nullable|image',
            'address' => 'nullable|string|max:255',
            'city' => 'nullable|string|max:255',
            'country' => 'nullable|string|max:255',
            'state' => 'nullable|string|max:255',
            'phone' => 'nullable|string|max:255',
            'zipCode' => 'nullable|integer',
            'password' => 'nullable|string|min:8',
        ));

        // Find the model
        $user = User::find($user->id);

        // Upload image
        if ($request->hasFile('avatar')) {
            //get filename with extension
            $filenamewithextension = $request->file('avatar')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('avatar')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename.'_'.time().'.'.$extension;

            //Upload File
            $request->file('avatar')->storeAs('public/images/profile_images/thumbnail', $filenametostore);

            //Resize image here
            $thumbnailpath = public_path('storage/images/profile_images/thumbnail/'.$filenametostore);
            $img = Image::make($thumbnailpath)->resize(100, 100, function($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($thumbnailpath);

            // Delete previous image from directory
            if ($user->avatar) {
                Storage::delete('public/images/profile_images/thumbnail/'.$user->avatar);
            }

            $user->avatar = $filenametostore;
        }

        if (isset($request->password)) {
            $user->password = Hash::make($request->new_password);
        }

        $user->name = $request->name;
        $user->address = $request->address;
        $user->city = $request->city;
        $user->country = $request->country;
        $user->state = $request->state;
        $user->zipCode = $request->zipCode;
        $user->phone = $request->phone;
        $user->save();

        return redirect()->back()->with('success', 'Client updated successfully.');
    }
}
