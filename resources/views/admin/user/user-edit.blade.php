@extends('admin.layouts.app')

@section('title')
    User edit
@endsection

@section('heading')
    User edit
@endsection

@section('breadcrumb')
    User edit
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! html_entity_decode(Session::get('success')) !!}
        </div>
    @endif
    <!-- Row created callback -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="card-title">Edit user</h4>
                        </div>
                        <div class="col-md-2 text-right">
                            <a href="{{ route('users') }}" class="btn btn-info"><i class="mdi mdi-step-backward"></i> Back to list</a>
                        </div>
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>
                    <form method="post" action="{{ route('users.profile.update', $user->id) }}" novalidate enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-lg-12 col-md-12">

                                <div class="form-group">
                                    <h5>Name<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="name" value="{{ $user->name }}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name" required data-validation-required-message="Please provide a name for the user">
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5>Image</h5>
                                    <div class="controls">
                                        @if ($user->avatar)<img src="{{ asset('storage/images/profile_images/thumbnail/'.$user->avatar) }}" height="100px" width="100px"> @endif
                                        <input type="file" name="avatar" class="form-control{{ $errors->has('avatar') ? ' is-invalid' : '' }}" accept="image/*">
                                        @error('avatar')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5>Phone</h5>
                                    <div class="controls">
                                        <input type="text" name="phone" value="{{ $user->phone }}" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" placeholder="Phone" required data-validation-required-message="This field is required">
                                        @error('phone')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5>Country</h5>
                                    <div class="controls">
                                        <input type="text" name="country" value="{{ $user->country }}" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" placeholder="Country" required data-validation-required-message="This field is required">
                                        @error('country')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5>State</h5>
                                    <div class="controls">
                                        <input type="text" name="state" value="{{ $user->state }}" class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}" placeholder="State" required data-validation-required-message="This field is required">
                                        @error('state')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5>City</h5>
                                    <div class="controls">
                                        <input type="text" name="city" value="{{ $user->city }}" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" placeholder="City" required data-validation-required-message="This field is required">
                                        @error('city')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5>Zip-Code</h5>
                                    <div class="controls">
                                        <input type="text" name="zipCode" value="{{ $user->zipCode }}" class="form-control{{ $errors->has('zipCode') ? ' is-invalid' : '' }}" placeholder="Zip-Code" required data-validation-required-message="This field is required">
                                        @error('zipCode')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5>Address</h5>
                                    <div class="controls">
                                        <input type="text" name="address" value="{{ $user->address }}" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" placeholder="Address" required data-validation-required-message="This field is required">
                                        @error('address')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group">
                                    <h5>Password </h5>
                                    <div class="controls">
                                        <input type="password" class="form-control @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}"  autocomplete="password" placeholder="Password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-12 col-md-12">
                                <hr>
                                <div class="text-xs-right">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <button type="reset" class="btn btn-inverse">Reset</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
