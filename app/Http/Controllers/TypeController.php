<?php

namespace App\Http\Controllers;

use App\Type;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = Type::all()->sortByDesc('id');

        return view('admin.type.types', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.type.type-create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate form data
        $request->validate([
            'title' => 'required|string|max:255',
        ]);

        // Create a model instance assign form data & save to DB
        $type = new Type();
        $type->title = $request->title;
        $type->save();

        return redirect()->back()->with('success', 'Type created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function show(Type $type)
    {
        // Get the model
        $type = Type::find($type->id);

        return view('admin.type.type-show', compact('type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function edit(Type $type)
    {
        // Get the model
        $type = Type::find($type->id);
        $edit = true;

        return view('admin.type.type-create-edit', compact('type', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Type $type)
    {
        // Validate form data
        $request->validate([
            'title' => 'required|string|max:255',
        ]);

        // Create a model instance assign form data & save to DB
        $type = Type::find($type->id);
        $type->title = $request->title;
        $type->save();

        return redirect()->back()->with('success', 'Type updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type $type)
    {
        // Get the model
        Type::destroy($type->id);

        return redirect()->back()->with('success', 'Type deleted successfully.');
    }
}
