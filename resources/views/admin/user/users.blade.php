@extends('admin.layouts.app')

@section('title')
    Users
@endsection

@section('heading')
    Users
@endsection

@section('breadcrumb')
    Users
@endsection

@section('content')
    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! html_entity_decode(Session::get('success')) !!}
    </div>
    @endif
    <!-- Row created callback -->
    <div class="row" id="user-page-wrapper">
        <div class="col-12">
            <div class="material-card card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="card-title">Clients</h4>
                        </div>
                        <div class="col-md-2 text-right">

                        </div>
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-hover border display" style="width:100%;">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Image</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Country</th>
                                <th>State</th>
                                <th>City</th>
                                <th>Zip-code</th>
                                <th>Address</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($clients as $client)
                            <tr>
                                <td>{{ $client->name }}</td>
                                <td><img src="{{ asset('storage/images/profile_images/thumbnail/'.$client->avatar) }}" height="100px" width="100px" style="border-radius: 50%"></td>
                                <td>{{ $client->email }}</td>
                                <td>{{ $client->phone }}</td>
                                <td>{{ $client->country }}</td>
                                <td>{{ $client->state }}</td>
                                <td>{{ $client->city }}</td>
                                <td>{{ $client->zipCode }}</td>
                                <td>{{ $client->address }}</td>
                                <td>
                                    <form action="{{ route('users.destroy', $client->id)}}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <a href="{{ route('users.show', $client->id)}}" class="btn btn-info btn-circle"><i class="fa fa-eye"></i> </a>
                                        <a href="{{ route('users.edit', $client->id)}}" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i> </a>
                                        <button type="submit" onclick="return confirm('Are you sure want to delete this data?')" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i> </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

