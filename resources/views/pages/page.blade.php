@extends('layouts.app')

@section('title')
    {{ $page->title }}
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h3 style="text-align: center; margin-top: 40px;">{{ $page->title }}</h3>

                <div class="card-body">
                    {!! html_entity_decode($page->content) !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
