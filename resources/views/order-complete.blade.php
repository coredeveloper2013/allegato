@extends('layouts.app')

@section('title')
    Order complete
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card" style="min-height: 500px">
                <div class="card-header">
                    Order complete
                </div>

                <div class="card-body">
                    <div class="alert alert-success" role="alert">
                        Your order has been placed successfully.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
