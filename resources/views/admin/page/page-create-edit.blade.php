@extends('admin.layouts.app')

@section('title')
    Page create
@endsection

@section('heading')
    Page @if(isset($edit)) edit @else create @endif
@endsection

@section('breadcrumb')
    Page @if(isset($edit)) edit @else create @endif
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! html_entity_decode(Session::get('success')) !!}
        </div>
    @endif
    <!-- Row created callback -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="card-title">@if(isset($edit)) Edit @else Create @endif page</h4>
                        </div>
                        <div class="col-md-2 text-right">
                            <a href="{{ route('pages.index') }}" class="btn btn-info"><i class="mdi mdi-step-backward"></i> Back to list</a>
                        </div>
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>
                    <form method="post" action="@if(isset($edit)) {{ route('pages.update', $page->id) }} @else {{ route('pages.store') }} @endif" novalidate enctype="multipart/form-data">
                        @csrf
                        @if(isset($edit))
                            @method('PUT')
                        @endif
                        <div class="row">
                            <div class="col-lg-12 col-md-12">

                                <div class="form-group validate">
                                    <h5>Title<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="title" value="@if(isset($edit)){{ $page->title }}@else{!! old('title') !!}@endif" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                        @error('title')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group validate">
                                    <h5>Content<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <textarea  name="content" class="form-control{{ $errors->has('content') ? ' is-invalid' : '' }} summernote" required data-validation-required-message="This field is required">@if(isset($edit)){{ $page->content }}@else{!! old('content') !!}@endif</textarea>
                                        @error('content')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group validate">
                                    <h5>Url<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">{{ url('/').'/' }}</div>
                                            </div>
                                            <input type="text" name="slug" class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required" value="@if(isset($edit)){{ $page->slug }}@else{!! old('slug') !!}@endif">
                                        </div>
                                        @error('slug')
                                        <span class="invalid-feedback" role="alert" style="display: block">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group validate">
                                    <h5>Showcase Page<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <fieldset>
                                            <div class="custom-control custom-radio">
                                                <input type="radio" name="status" value="1" required="" id="showcase1" class="custom-control-input" aria-invalid="false" @if(isset($edit)) @if($page->status === 1) checked @endif @endif>
                                                <label class="custom-control-label" for="showcase1">Active</label>
                                            </div>
                                        </fieldset>
                                        <fieldset>
                                            <div class="custom-control custom-radio">
                                                <input type="radio"  name="status" value="0" id="showcase" class="custom-control-input" aria-invalid="false" @if(isset($edit)) @if($page->status === 0) checked @endif @endif>
                                                <label class="custom-control-label" for="showcase">Inactive</label>
                                            </div>
                                            @error('status')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </fieldset>
                                        <div class="help-block"></div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <hr>
                                    </div>
                                    <div class="col-md-10">
                                        <h4 class="card-title">SEO</h4>
                                    </div>
                                    <div class="col-md-2 text-right">

                                    </div>
                                    <div class="col-12">
                                        <hr>
                                    </div>
                                </div>

                                <div class="form-group validate">
                                    <h5>Meta Title<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <input type="text" name="meta_title" value="@if(isset($edit)){{ $page->meta_title }}@else{!! old('meta_title') !!}@endif" class="form-control{{ $errors->has('meta_title') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                        @error('meta_title')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group validate">
                                    <h5>Meta Description<span class="text-danger">*</span></h5>
                                    <div class="controls">
                                        <textarea  name="meta_description" class="form-control{{ $errors->has('meta_description') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">@if(isset($edit)){{ $page->meta_description }}@else{!! old('meta_description') !!}@endif</textarea>
                                        @error('meta_description')
                                        <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-12 col-md-12">
                                <hr>
                                <div class="text-xs-right">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <button type="reset" class="btn btn-inverse">Reset</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('.summernote').summernote({
            height: 300, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
            focus: false // set focus to editable area after initializing summernote
        });
    </script>
@endsection
