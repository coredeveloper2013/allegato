@extends('admin.layouts.app')

@section('title')
    User show
@endsection

@section('heading')
    Users show
@endsection

@section('breadcrumb')
    Users show
@endsection

@section('content')
    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        {!! html_entity_decode(Session::get('success')) !!}
    </div>
    @endif
    <!-- Row created callback -->
    <div class="row" id="user-page-wrapper">
        <div class="col-12">
            <div class="material-card card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="card-title">{{ $user->name }}</h4>
                        </div>
                        <div class="col-md-2 text-right">
                            <a href="{{ route('users') }}" class="btn btn-info"><i class="mdi mdi-step-backward"></i> Back to list</a>
                        </div>
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-xl-3">
                            <!-- Nav tabs -->
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link active" id="v-pills-order-tab" data-toggle="pill" href="#v-pills-order" role="tab" aria-controls="v-pills-order" aria-selected="true">Clients Order</a>
                                <a class="nav-link" id="v-pills-addresses-tab" data-toggle="pill" href="#v-pills-addresses" role="tab" aria-controls="v-pills-addresses" aria-selected="false">Clients addresses</a>
                                <a class="nav-link" id="v-pills-payment-tab" data-toggle="pill" href="#v-pills-payment" role="tab" aria-controls="v-pills-payment" aria-selected="false">Clients payment</a>
                                <a class="nav-link" id="v-pills-invoice-tab" data-toggle="pill" href="#v-pills-invoice" role="tab" aria-controls="v-pills-invoice" aria-selected="false">Clients invoices</a>
                            </div>
                        </div>
                        <div class="col-lg-8 col-xl-9">
                            <div class="tab-content p-4" id="v-pills-tabContent">
                                <div class="tab-pane fade active show" id="v-pills-order" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                    <div class="table-responsive">
                                        <table id="zero_config" class="table table-striped table-hover border display" style="width:100%;">
                                            <thead>
                                            <tr>
                                                <th>Receiver name</th>
                                                <th>Country</th>
                                                <th>State</th>
                                                <th>City</th>
                                                <th>Zip-code</th>
                                                <th>Address</th>
                                                <th>Price</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($user->orders as $order)
                                                <tr>
                                                    <td>{{ $order->receiver_contactname }}</td>
                                                    <td>{{ $order->receiver_country }}</td>
                                                    <td>{{ $order->receiver_province }}</td>
                                                    <td>{{ $order->receiver_town }}</td>
                                                    <td>{{ $order->receiver_postcode }}</td>
                                                    <td>{{ $order->receiver_address }}</td>
                                                    <td>€ {{ $order->price }}</td>
                                                    <td>
                                                        <form action="{{ route('invoices.store')}}" id="invoices-create" method="post">
                                                            @csrf
                                                            <input type="hidden" name="order_id" value="{{ $order->id }}">
                                                        </form>
                                                        <form action="{{ route('orders.destroy', $order->id)}}" method="post">
                                                            @csrf
                                                            @method('DELETE')
                                                            <a href="{{ route('orders.show', $order->id)}}" class="btn btn-info btn-circle"><i class="fa fa-eye"></i> </a>
                                                            <a href="{{ route('orders.edit', $order->id)}}" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i> </a>
                                                            <a href="@if($order->invoice){{ route('invoices.show', $order->id)}}@else javascript:void(0) @endif" @if(!$order->invoice) onclick="event.preventDefault();document.getElementById('invoices-create').submit();" @endif class="btn btn-primary btn-circle"><i class="fas fa-file-alt"></i> </a>
                                                            <button type="submit" onclick="return confirm('Are you sure want to delete this data?')" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i> </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="v-pills-addresses" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                    <div class="table-responsive">
                                        <table id="zero_config" class="table table-striped table-hover border display" style="width:100%;">
                                            <thead>
                                            <tr>
                                                <th>Phone</th>
                                                <th>Country</th>
                                                <th>State</th>
                                                <th>City</th>
                                                <th>Zip-code</th>
                                                <th>Address</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($user->addresses as $address)
                                                <tr>
                                                    <td>{{ $address->phone }}</td>
                                                    <td>{{ $address->country }}</td>
                                                    <td>{{ $address->state }}</td>
                                                    <td>{{ $address->city }}</td>
                                                    <td>{{ $address->zipCode }}</td>
                                                    <td>{{ $address->address }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="v-pills-payment" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                    <p> Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica.</p>
                                    Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Mustache cliche tempor, williamsburg carles vegan helvetica.
                                </div>
                                <div class="tab-pane fade" id="v-pills-invoice" role="tabpanel" aria-labelledby="v-pills-invoice-tab">
                                    <div class="table-responsive">
                                        <table id="zero_config" class="table table-striped table-hover border display" style="width: 100%">
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Order No</th>
                                                <th>Customer Name</th>
                                                <th>Balance</th>
                                                <th>Actions</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($user->orders as $order)
                                                @if($order->invoice)
                                                <tr>
                                                    <td>{{ $order->invoice->created_at }}</td>
                                                    <td>{{ $order->id }}</td>
                                                    <td>{{ $order->user->name }}</td>
                                                    <td>€ {{ $order->price }}</td>
                                                    <td>
                                                        <form action="https://www.app.fattura24.com/api/v0.3/GetFile" id="invoices-download" method="post">
                                                            @csrf
                                                            <input type="hidden" name="apiKey" value="{{ env('FATTURA24_API_KEY') }}">
                                                            <input type="hidden" name="docId" value="{{ $order->invoice->docId }}">
                                                        </form>
                                                        <form action="{{ route('invoices.destroy', $order->invoice->id)}}" method="post">
                                                            @csrf
                                                            @method('DELETE')
                                                            <a href="{{ route('invoices.show', $order->invoice->id)}}" class="btn btn-info btn-circle"><i class="fa fa-eye"></i> </a>
                                                            <a href="javascript:void(0)" onclick="event.preventDefault();document.getElementById('invoices-download').submit();" class="btn btn-primary btn-circle"><i class="fas fa-file-pdf"></i> </a>
                                                            <button type="submit" onclick="return confirm('Are you sure want to delete this data?')" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i> </button>
                                                        </form>
                                                    </td>
                                                </tr>
                                                @endif
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

