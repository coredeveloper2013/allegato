<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiteController extends Controller
{
    /**
     * Show the calculation form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function calculate(Request $request)
    {
        // convert from degrees to radians
        $latFrom = deg2rad(request()->location_from_lat);
        $lonFrom = deg2rad(request()->location_from_long);
        $latTo = deg2rad(request()->location_to_lat);
        $lonTo = deg2rad(request()->location_to_long);

        $lonDelta = $lonTo - $lonFrom;
        $a = pow(cos($latTo) * sin($lonDelta), 2) +
            pow(cos($latFrom) * sin($latTo) - sin($latFrom) * cos($latTo) * cos($lonDelta), 2);
        $b = sin($latFrom) * sin($latTo) + cos($latFrom) * cos($latTo) * cos($lonDelta);

        $angle = atan2(sqrt($a), $b);
        $distance = $angle * 6371;

        return view('calculate', compact('request', 'distance'));
    }

    /**
     * Show the address form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function address(Request $request)
    {
        return view('address', compact('request'));
    }

    /**
     * Show the payment form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function payment(Request $request)
    {
        return view('payment', compact('request'));
    }

    /**
     * Show the order success page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function orderSuccess(Request $request)
    {
        return view('success', compact('request'));
    }

    /**
     * Show the order invoice page.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function orderInvoice(Request $request)
    {
        return view('invoice', compact('request'));
    }
}
