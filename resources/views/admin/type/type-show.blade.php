@extends('admin.layouts.app')

@section('title')
    Type show
@endsection

@section('heading')
    Type show
@endsection

@section('breadcrumb')
    Type show
@endsection

@section('content')
    <!-- Row created callback -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="card-title">Type</h4>
                        </div>
                        <div class="col-md-2 text-right">
                            <a href="{{ route('types.index') }}" class="btn btn-info"><i class="mdi mdi-step-backward"></i> Back to list</a>
                        </div>
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="file_export" class="table table-striped table-hover border display" style="width: 100%">
                            <tbody>
                                <tr>
                                    <th>Title</th>
                                    <td>{{ $type->title }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
