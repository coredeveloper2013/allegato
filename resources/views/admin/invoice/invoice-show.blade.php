@extends('admin.layouts.app')

@section('title')
    Invoice show
@endsection

@section('heading')
    Invoice show
@endsection

@section('breadcrumb')
    Invoice show
@endsection

@section('content')
    <!-- Row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="mb-0 text-white">Form with view only</h4>
                </div>
                <form class="form-horizontal">
                    <div class="form-body">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-10">
                                    <h4 class="card-title">Invoice show</h4>
                                </div>
                                <div class="col-md-2 text-right">
                                    <a href="{{ route('invoices.index') }}" class="btn btn-info"><i class="mdi mdi-step-backward"></i> Back to list</a>
                                </div>
                            </div>
                        </div>
                        <hr class="mt-0 mb-5">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Date:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $invoice->created_at }} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Order No:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $invoice->order->id }} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Customer Name:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $invoice->order->user->name }} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Balance:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static">€ {{ $invoice->order->price }} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Return Code:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $invoice->returnCode }} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Description:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $invoice->description }} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Document Id:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $invoice->docId }} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Document Number:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $invoice->docNumber }} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Row -->
@endsection
