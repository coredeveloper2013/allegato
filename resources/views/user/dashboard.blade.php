@extends('layouts.app')

@section('content')
    <div id="this_page_wrapper">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Dashboard</div>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-3">
                                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                        <a class="nav-link active" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="true">Profile</a>
                                        <a class="nav-link" id="v-pills-payment-tab" data-toggle="pill" href="#v-pills-payment" role="tab" aria-controls="v-pills-payment" aria-selected="false">Payment methods</a>
                                        <a class="nav-link" id="v-pills-password-tab" data-toggle="pill" href="#v-pills-password" role="tab" aria-controls="v-pills-password" aria-selected="false">Password</a>
                                        <a class="nav-link" id="v-pills-addresses-tab" data-toggle="pill" href="#v-pills-addresses" role="tab" aria-controls="v-pills-addresses" aria-selected="false">Saved addresses</a>
                                        <a class="nav-link" id="v-pills-order-tab" data-toggle="pill" href="#v-pills-order" role="tab" aria-controls="v-pills-order" aria-selected="false">Order history</a>
                                        <a class="nav-link" id="v-pills-prepaid-tab" data-toggle="pill" href="#v-pills-prepaid" role="tab" aria-controls="v-pills-prepaid" aria-selected="false">Prepaid</a>
                                        <a class="nav-link" id="v-pills-address-book-tab" data-toggle="pill" href="#v-pills-address-book" role="tab" aria-controls="v-pills-address-book" aria-selected="false">Address book</a>
                                    </div>
                                </div>
                                <div class="col-9">
                                    <div class="tab-content" id="v-pills-tabContent">
                                        <div class="tab-pane fade show active" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                            <h4>Update profile</h4>
                                            <hr>
                                            <p class="alert alert-success profile-success-msg" style="display: none"></p>
                                            <ul class="alert alert-danger profile-error-msg" style="display: none"></ul>
                                            <form action="{{ route('users.update', auth()->user()->id) }}" id="profile-update-form" @submit.prevent="updateProfile()">
                                                @csrf
                                                <input type="hidden" name="_method" value="PUT">
                                                <div class="form-group">
                                                    <label for="avatar">Profile picture:</label>
                                                    <img :src="'storage/images/profile_images/thumbnail/' + avatar" v-if="avatar" style="border-radius: 50%">
                                                    <input type="file" ref="file" accept="image/*" class="form-control" :class="errors.avatar ? 'is-invalid':''" name="avatar" @change="uploadImage()">
                                                    <span class="invalid-feedback" role="alert" v-if="errors.avatar">
                                                        <strong>@{{ errors.avatar[0] }}</strong>
                                                    </span>
                                                </div>
                                                <div class="form-group">
                                                    <label for="phone">Name:</label>
                                                    <input type="text" class="form-control" name="name" value="{{ $user->name }}" placeholder="Your name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="country">Select country:</label>
                                                    <select class="form-control" name="country">
                                                        <option value="">Select country</option>
                                                        <option value="Bangladesh" @if ($user->country == 'Bangladesh') selected @endif>Bangladesh</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="state">Select state:</label>
                                                    <select class="form-control" name="state">
                                                        <option value="">Select state</option>
                                                        <option value="Dhaka" @if ($user->state == 'Dhaka') selected @endif>Dhaka</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="city">Select city:</label>
                                                    <select class="form-control" name="city">
                                                        <option value="">Select city</option>
                                                        <option value="Dhaka" @if ($user->city == 'Dhaka') selected @endif>Dhaka</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="zipCode">Zip-Code:</label>
                                                    <input type="number" class="form-control" name="zipCode" value="{{ $user->zipCode }}" placeholder="Your Zip-Code">
                                                </div>
                                                <div class="form-group">
                                                    <label for="phone">Phone:</label>
                                                    <input type="text" class="form-control" name="phone" value="{{ $user->phone }}" placeholder="Your phone">
                                                </div>
                                                <div class="form-group">
                                                    <label for="address">Address:</label>
                                                    <textarea name="address" class="form-control" rows="5" placeholder="Your detail address">{{ $user->address }}</textarea>
                                                </div>
                                                <button type="submit" class="btn btn-default">Submit</button>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="v-pills-payment" role="tabpanel" aria-labelledby="v-pills-payment-tab">
                                            <h4>Payment method</h4>
                                            <hr>
                                        </div>
                                        <div class="tab-pane fade" id="v-pills-password" role="tabpanel" aria-labelledby="v-pills-password-tab">
                                            <h4>Update password</h4>
                                            <hr>
                                            <p class="alert alert-success psw-success-msg" style="display: none"></p>
                                            <ul class="alert alert-danger psw-error-msg" style="display: none"></ul>
                                            <form action="{{ route('user.password.update') }}" id="password-update-form" @submit.prevent="updatePassword()">
                                                @csrf
                                                <input type="hidden" name="_method" value="PUT">
                                                <div class="form-group">
                                                    <label for="pwd">Password:</label>
                                                    <input type="password" class="form-control" name="password" placeholder="Old password">
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd">New Password:</label>
                                                    <input type="password" class="form-control" name="new_password" placeholder="New password">
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd">Password:</label>
                                                    <input type="password" class="form-control" name="new_password_confirmation" placeholder="Confirm new password">
                                                </div>
                                                <button type="submit" class="btn btn-default">Submit</button>
                                            </form>
                                        </div>
                                        <div class="tab-pane fade" id="v-pills-addresses" role="tabpanel" aria-labelledby="v-pills-addresses-tab">
                                            <h4 style="float: left">Address</h4>
                                            <!-- Trigger the modal with a button -->
                                            <button style="float: right" type="button" class="btn btn-success" data-toggle="modal" data-target="#addAddress"><i class="fa fa-plus"></i> Address</button>
                                            <br><hr>
                                            <table class="table table-bordered table-striped" style="width:100%">
                                                <thead>
                                                <tr>
                                                    <th>country</th>
                                                    <th>state</th>
                                                    <th>city</th>
                                                    <th>zipCode</th>
                                                    <th>phone</th>
                                                    <th>address</th>
                                                    <th style="width: 100px;">
                                                        action
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr v-for="address in addresses">
                                                    <td>@{{ address.country }}</td>
                                                    <td>@{{ address.state }}</td>
                                                    <td>@{{ address.city }}</td>
                                                    <td>@{{ address.zipCode }}</td>
                                                    <td>@{{ address.phone }}</td>
                                                    <td>@{{ address.address }}</td>
                                                    <td>
                                                        <div class="table-data-feature">
                                                            <a href="#" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#editAddress" @click.prevent="showEditFormData(address.id)">
                                                                <i class="fa fa-edit"></i>
                                                            </a>
                                                            <a href="#" class="btn btn-danger btn-sm" @click.prevent="deleteAddress(address.id)">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="tab-pane fade" id="v-pills-order" role="tabpanel" aria-labelledby="v-pills-order-tab">
                                            <h4>Order history</h4>
                                            <hr>
                                        </div>
                                        <div class="tab-pane fade" id="v-pills-prepaid" role="tabpanel" aria-labelledby="v-pills-prepaid-tab">
                                            <h4>Prepaid</h4>
                                            <hr>
                                        </div>
                                        <div class="tab-pane fade" id="v-pills-address-book" role="tabpanel" aria-labelledby="v-pills-address-book-tab">
                                            <h4>Address book</h4>
                                            <hr>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="addAddress" role="dialog">
                            <div class="modal-dialog modal-lg">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Add Address</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <p class="alert alert-success" v-if="success">@{{ success }}</p>
                                        <form action="{{ route('addresses.store') }}" id="address-create-form" @submit.prevent="addAddress()">
                                            @csrf
                                            <div class="form-group">
                                                <label for="country">Select country:</label>
                                                <select class="form-control" name="country">
                                                    <option value="">Select country</option>
                                                    <option value="Bangladesh">Bangladesh</option>
                                                </select>
                                                <p class="alert alert-danger" v-if="errors.country">@{{ errors.country[0] }}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="state">Select state:</label>
                                                <select class="form-control" name="state">
                                                    <option value="">Select state</option>
                                                    <option value="Dhaka">Dhaka</option>
                                                </select>
                                                <p class="alert alert-danger" v-if="errors.state">@{{ errors.state[0] }}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="city">Select city:</label>
                                                <select class="form-control" name="city">
                                                    <option value="">Select city</option>
                                                    <option value="Dhaka">Dhaka</option>
                                                </select>
                                                <p class="alert alert-danger" v-if="errors.city">@{{ errors.city[0] }}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="zipCode">Zip-Code:</label>
                                                <input type="number" class="form-control" name="zipCode" placeholder="Your Zip-Code">
                                                <p class="alert alert-danger" v-if="errors.zipCode">@{{ errors.zipCode[0] }}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="phone">Phone:</label>
                                                <input type="text" class="form-control" name="phone" placeholder="Your phone">
                                                <p class="alert alert-danger" v-if="errors.phone">@{{ errors.phone[0] }}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="address">Address:</label>
                                                <textarea name="address" class="form-control" rows="5" placeholder="Your detail address"></textarea>
                                                <p class="alert alert-danger" v-if="errors.address">@{{ errors.address[0] }}</p>
                                            </div>
                                            <button type="submit" class="btn btn-default">Submit</button>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <!-- Modal -->
                        <div class="modal fade" id="editAddress" role="dialog">
                            <div class="modal-dialog modal-lg">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Edit Address</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <p class="alert alert-success" v-if="success">@{{ success }}</p>
                                        <form action="{{ route('addresses.store') }}" id="address-edit-form" @submit.prevent="updateAddress(address.id)">
                                            @csrf
                                            <input type="hidden" name="_method" value="PUT">
                                            <div class="form-group">
                                                <label for="country">Select country:</label>
                                                <select class="form-control" v-model="address.country">
                                                    <option value="Bangladesh">Bangladesh</option>
                                                </select>
                                                <p class="alert alert-danger" v-if="errors.country">@{{ errors.country[0] }}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="state">Select state:</label>
                                                <select class="form-control" v-model="address.state">
                                                    <option value="Dhaka">Dhaka</option>
                                                </select>
                                                <p class="alert alert-danger" v-if="errors.state">@{{ errors.state[0] }}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="city">Select city:</label>
                                                <select class="form-control" v-model="address.city">
                                                    <option value="Dhaka">Dhaka</option>
                                                </select>
                                                <p class="alert alert-danger" v-if="errors.city">@{{ errors.city[0] }}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="zipCode">Zip-Code:</label>
                                                <input type="number" class="form-control" v-model="address.zipCode" placeholder="Your Zip-Code">
                                                <p class="alert alert-danger" v-if="errors.zipCode">@{{ errors.zipCode[0] }}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="phone">Phone:</label>
                                                <input type="text" class="form-control" v-model="address.phone" placeholder="Your phone">
                                                <p class="alert alert-danger" v-if="errors.phone">@{{ errors.phone[0] }}</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="address">Address:</label>
                                                <textarea v-model="address.address" class="form-control" rows="5" placeholder="Your detail address"></textarea>
                                                <p class="alert alert-danger" v-if="errors.address">@{{ errors.address[0] }}</p>
                                            </div>
                                            <button type="submit" class="btn btn-default">Submit</button>
                                        </form>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')

<script>
    new Vue({
        el: '#this_page_wrapper',
        data: {
            avatar: '<?php echo $user->avatar ?>',
            address: {
                avatar: null,
                country: null,
                state: null,
                city: null,
                zipCode: null,
                phone: null,
                address: null,
            },
            addresses: '<?php echo $addresses ?>',
            errors: [],
            success: null,
        },
        methods: {
            addAddress() {
                var _this = this;

                // Get the form.
                var form = $("#address-create-form");

                // Serialize the form data.
                var formData = $(form).serialize();

                // Submit the form using AJAX.
                $.ajax({
                    type: 'POST',
                    url: $(form).attr('action'),
                    data: formData,
                    success: function(data){
                        if (data.errors) {
                            _this.success = null;
                            _this.errors = data.errors;
                        } else {
                            _this.errors = [];
                            _this.addresses.push(data);
                            $(form).trigger('reset');
                            // Show success message
                            _this.success = 'Address added successfully.';
                            setTimeout(function () {
                                $('.alert-success').hide();
                            }, 3000);
                        }
                    }
                });
            },
            updateAddress(id) {
                var _this = this;

                // Get the form
                var form = $('#address-edit-form');

                _this.address['_method'] = 'PUT';
                _this.address['_token'] = $('input[name=_token]').val();

                // Submit the form using AJAX.
                $.ajax({
                    type: 'POST',
                    url: $(form).attr('action') + '/' + id,
                    data: _this.address,
                    success: function(data){
                        if (data.errors) {
                            _this.errors = data.errors;
                            _this.success = null;
                        } else {
                            _this.errors = [];
                            _this.success = 'Address updated successfully.';
                            var index = _this.addresses.findIndex(x => x.id === id);
                            _this.addresses.splice(index, 1, data);
                            setTimeout(function () {
                                $('.alert-success').hide();
                            }, 3000);
                        }
                    }
                });
            },
            showEditFormData(id) {
                var index = this.addresses.findIndex(x => x.id === id);
                this.address = this.addresses[index];
            },
            deleteAddress(id) {
                var _this = this;

                // Submit the form using AJAX.
                $.ajax({
                    type: 'POST',
                    url: 'addresses/' + id,
                    data: {'_method': 'DELETE', '_token': $('input[name=_token]').val()},
                    success: function(data){
                        if (data.errors) {
                            _this.errors = data.errors;
                        } else {
                            _this.addresses.splice(_this.addresses.findIndex(x => x.id === id), 1);
                        }
                    }
                });
            },
            updatePassword() {
                // Get the form
                var form = $('#password-update-form');

                // Submit the form using AJAX.
                $.ajax({
                    type: 'POST',
                    url: $(form).attr('action'),
                    data: $(form).serialize(),
                    success: function(data){
                        $('.psw-error-msg').html('');
                        if (data.errors) {
                            $('.psw-success-msg').hide();
                            $('.psw-error-msg').show();
                            $.each(data.errors, function(key, value){
                                $('.psw-error-msg').append('<li>'+value+'</li>');
                            });
                        } else {
                            //reset the form
                            $(form).trigger('reset');
                            // Show success message
                            $('.psw-error-msg').hide();
                            $('.psw-success-msg').show().text('Password updated successfully.');
                            setTimeout(function () {
                                $('.psw-success-msg').hide();
                            }, 3000);
                        }
                    }
                });
            },
            uploadImage() {
                let _this = this;
                var file = this.$refs.file.files[0];
                if (file['type'] === 'image/jpg' || file['type'] === 'image/png' || file['type'] === 'image/jpeg' || file['type'] === 'image/tiff' || file['type'] === 'image/gif' || file['type'] === 'image/bmp' || file['type'] === 'image/ppm' || file['type'] === 'image/pgm' || file['type'] === 'image/pbm ' || file['type'] === 'image/pnm' || file['type'] === 'image/webp' || file['type'] === 'image/pjp'  || file['type'] === 'image/jfif' || file['type'] === 'image/pjpeg' || file['type'] === 'image/svgz' || file['type'] === 'image/svg' || file['type'] === 'image/ico' || file['type'] === 'image/xbm' || file['type'] === 'image/dib') {

                    let formData = new FormData();
                    formData.append('avatar', this.$refs.file.files[0]);
                    formData.append('_token', '{{ csrf_token() }}');
                    formData.append('_method', 'PUT');

                    // Submit the form using AJAX.
                    $.ajax({
                        type: 'POST',
                        url: 'users-upload-image',
                        data: formData,
                        contentType: false,
                        processData: false,
                        cache: false,
                        success: function(data){
                            if (data.errors) {
                                _this.errors = data.errors;
                            } else {
                                _this.avatar = data;
                            }
                        }
                    });
                }else {
                    alert("File must be image type!");
                }
            },
            updateProfile() {
                // Get the form
                var form = $('#profile-update-form');

                // Submit the form using AJAX.
                $.ajax({
                    type: 'POST',
                    url: $(form).attr('action'),
                    data: $(form).serialize(),
                    success: function(data){
                        $('.profile-error-msg').html('');
                        if (data.errors) {
                            $('.profile-success-msg').hide();
                            $('.profile-error-msg').show();
                            $.each(data.errors, function(key, value){
                                $('.profile-error-msg').append('<li>'+value+'</li>');
                            });
                        } else {
                            // Show success message
                            $('.profile-error-msg').hide();
                            $('.profile-success-msg').show().text('Profile updated successfully.');
                            setTimeout(function () {
                                $('.profile-success-msg').hide();
                            }, 3000);
                        }
                    }
                });
            }
        },
        mounted() {
            this.addresses = JSON.parse(this.addresses);
        }
    });
</script>

@endsection
