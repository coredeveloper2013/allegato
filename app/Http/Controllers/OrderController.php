<?php

namespace App\Http\Controllers;

use App\Order;
use Stripe\Charge;
use Stripe\Stripe;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('store');
        $this->middleware('auth:admin')->except('store');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here: https://dashboard.stripe.com/account/apikeys
        Stripe::setApiKey(env('STRIPE_SECRET_KEY'));

        // Create a new charge
        $charge = Charge::create([
            'amount' => $request->amount * 100,
            'currency' => 'eur',
            'description' => 'Example charge',
            'source' => $request->token,
        ]);

        return redirect()->route('orders.complete', compact('charge'))->with('success', 'Your order placed successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        Order::destroy($order->id);

        return redirect()->back()->with('success', 'Order deleted successfully.');
    }

    /**
     * Display a order complete status.
     *
     * @return \Illuminate\Http\Response
     */
    public function complete()
    {
        if (!isset($charge)) {
            abort(404);
        }

        return view('order-complete');
    }
}
