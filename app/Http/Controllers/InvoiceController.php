<?php

namespace App\Http\Controllers;

use App\Invoice;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\In;

class InvoiceController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get all resources
        $invoices = Invoice::all()->sortByDesc('id');

        return view('admin.invoice.invoices', compact('invoices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $xmlstring = '
            <Fattura24>
              <Document>
                <DocumentType>I</DocumentType>
                <CustomerName>Mario Rossi</CustomerName>
                <CustomerAddress>Via Alberti 8</CustomerAddress>
                <CustomerPostcode>06122</CustomerPostcode>
                <CustomerCity>Perugia</CustomerCity>
                <CustomerProvince>PG</CustomerProvince>
                <CustomerCountry></CustomerCountry>
                <CustomerFiscalCode>MARROS66C44G217W</CustomerFiscalCode>
                <CustomerVatCode>03912377542</CustomerVatCode>
                <CustomerCellPhone>335123456789</CustomerCellPhone>
                <CustomerEmail>smsadhin123@gmail.com</CustomerEmail>
                <DeliveryName>Mario Rossi</DeliveryName>
                <DeliveryAddress>Via Alberti 8</DeliveryAddress>
                <DeliveryPostcode>06122</DeliveryPostcode>
                <DeliveryCity>Perugia</DeliveryCity>
                <DeliveryProvince>PG</DeliveryProvince>
                <DeliveryCountry></DeliveryCountry>
                <Object>Oggetto del documento</Object>
                <TotalWithoutTax>900.00</TotalWithoutTax>
                <PaymentMethodName>Banca Popolare di.....</PaymentMethodName>
                <PaymentMethodDescription>IBAN: IT02L1234512345123456789012</PaymentMethodDescription>
                <VatAmount>198.00</VatAmount>
                <Total>1098.00</Total>
                <FootNotes>Vi ringraziamo per la preferenza accordataci</FootNotes>
                <SendEmail>true</SendEmail>
                <UpdateStorage>1</UpdateStorage>
                <F24InvoiceId>12345</F24InvoiceId>
                <IdTemplate>123</IdTemplate>
                <CustomField1></CustomField1>
                <CustomField2></CustomField2>
                <Payments>
                  <Payment>
                    <Date>2016-02-23</Date>
                    <Amount>2135</Amount>
                    <Paid>true</Paid>
                  </Payment>
                </Payments>    
                <Rows>
                  <Row>
                    <Code>0001</Code>
                    <Description>PULIZIA NUM. DUE FINESTRE A DUE ANTE E DUE MANI DI SMALTO ALL’ACQUA COMPRESI IMBOTTI E CASSETTONI AVVOLGIBILI</Description>
                    <Qty>2</Qty>
                    <Um></Um>
                    <Price>200.00</Price>
                    <Discounts></Discounts>
                    <VatCode>22</VatCode>
                    <VatDescription>IVA 22%</VatDescription>
                  </Row>
                  <Row>
                    <Code>0002</Code>
                    <Description>PULIZIA  NUM. DUE FINESTRONI A DUE ANTE E DUE MANI DI SMALTO ALL’ACQUA COMPRESI IMBOTTI E CASSETTONI AVVOLGIBILI</Description>
                    <Qty>2</Qty>
                    <Um></Um>
                    <Price>250.00</Price>
                    <Discounts></Discounts>
                    <VatCode>22</VatCode>
                    <VatDescription>IVA 22%</VatDescription>
                  </Row>
                </Rows>
              </Document>
            </Fattura24>
        ';

        $xw = xmlwriter_open_memory();
        xmlwriter_start_document($xw, '1.0', 'UTF-8');
        xmlwriter_text($xw, $xmlstring);

        $xml = xmlwriter_output_memory($xw);

        // Make the http call to fattura24 api
        $xml_res = $this->createDocument('creates', $xml);

        /* LEGGO I DATI RICEVUTI DA FATTURA24 */
        $xml = simplexml_load_string($xml_res);

        $invoice = new Invoice();
        $invoice->returnCode = $xml->returnCode;
        $invoice->description = $xml->description;
        $invoice->docId = $xml->docId;
        $invoice->docNumber = $xml->docNumber;
        $invoice->order_id = $request->order_id;
        $invoice->save();

        return redirect(route('invoices.show', $invoice->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function show(Invoice $invoice)
    {
        // Get the model
        $invoice = Invoice::find($invoice->id);

        return view('admin.invoice.invoice-show', compact('invoice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function edit(Invoice $invoice)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invoice $invoice)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Invoice  $invoice
     * @return \Illuminate\Http\Response
     */
    public function destroy(Invoice $invoice)
    {
        // Get the model
        Invoice::destroy($invoice->id);

        return redirect()->back()->with('success', 'Invoice deleted successfully.');
    }

    /**
     * Make the http call to fattura24 api.
     *
     * @return \Illuminate\Http\Response
     */
    public function createDocument($action, $xml)
    {
        $efatt_api_key = env('FATTURA24_API_KEY');

        switch ($action){
            // Create map with request parameters
            case 'test': $action = '/TestKey'; $params = array ('apiKey' => $efatt_api_key); break;
            case 'creates': $action = '/SaveDocument'; $params = array ('apiKey' => $efatt_api_key, 'xml' => $xml);break;
        }


        $api_url = 'https://www.app.fattura24.com/api/v0.3';

        // Build Http query using params
        $query = http_build_query ($params);

        // Create Http context details
        $contextData = array (
            'method' => 'POST',
            'header' => "Content-Type: application/x-www-form-urlencoded\r\n".
                "Connection: close\r\n".
                "Content-Length: ".strlen($query)."\r\n",
            'content'=> $query );

        // Create context resource for our request
        $context = stream_context_create (array ( 'http' => $contextData ));

        // Read page rendered as result of your POST request
        $result =  file_get_contents (
            $api_url.$action,  // page url
            false,
            $context);


        // Server response is now stored in $result variable so you can process it
        $result = html_entity_decode($result);
        return $result;
    }
}
