@extends('admin.layouts.app')

@section('title')
  Pages
@endsection

@section('heading')
    Pages
@endsection

@section('breadcrumb')
    Pages
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! html_entity_decode(Session::get('success')) !!}
        </div>
    @endif
    <!-- Row created callback -->
    <div class="row">
        <div class="col-12">
            <div class="material-card card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="card-title">Pages</h4>
                        </div>
                        <div class="col-md-2 text-right">
                            <a href="{{ route('pages.create') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add New</a>
                        </div>
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-hover border display" style="width: 100%">
                            <thead>
                            <tr>
                                <th>Title</th>
                                <th>Content</th>
                                <th>Slug</th>
                                <th>Url</th>
                                <th>Meta title</th>
                                <th>Meta description</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pages as $page)
                                <tr>
                                    <td>{{ $page->title }}</td>
                                    <td width="30%">{{ substr(strip_tags($page->content), 0, 375) }}.....</td>
                                    <td>{{ $page->slug }}</td>
                                    <td>{{ url($page->slug) }}</td>
                                    <td>{{ $page->meta_title }}</td>
                                    <td>{{ $page->meta_description }}</td>
                                    <td>
                                        @if($page->status === 1)
                                            <button type="button" class="btn waves-effect waves-light btn-rounded btn-success">Active</button>
                                        @else
                                            <button type="button" class="btn waves-effect waves-light btn-rounded btn-danger">Inactive</button>
                                        @endif
                                    </td>
                                    <td>
                                        <form action="{{ route('pages.destroy', $page->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <a href="{{ route('pages.show', $page->id)}}" class="btn btn-info btn-circle"><i class="fa fa-eye"></i> </a>
                                            <a href="{{ route('pages.edit', $page->id)}}" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i> </a>
                                            <button type="submit" onclick="return confirm('Are you sure want to delete this data?')" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i> </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
