@extends('admin.layouts.app')

@section('title')
    Rate create
@endsection

@section('heading')
    Rate @if(isset($edit)) edit @else create @endif
@endsection

@section('breadcrumb')
    Rate @if(isset($edit)) edit @else create @endif
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! html_entity_decode(Session::get('success')) !!}
        </div>
    @endif
    <!-- Row -->
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="card-title">@if(isset($edit)) Edit @else Create @endif rate</h4>
                        </div>
                        <div class="col-md-2 text-right">
                            <a href="{{ route('rates.index') }}" class="btn btn-info"><i class="mdi mdi-step-backward"></i> Back to list</a>
                        </div>
                    </div>
                </div>
                <hr>
                <form method="post" class="form-horizontal" action="@if(isset($edit)) {{ route('rates.update', $rate->id) }} @else {{ route('rates.store') }} @endif" novalidate enctype="multipart/form-data">
                    @csrf
                    @if(isset($edit))
                        @method('PUT')
                    @endif
                    <div class="card-body">
                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label for="carrier_id" class="col-sm-3 text-right control-label col-form-label">Courier<span class="text-danger">*</span></label>
                                    <div class="controls col-sm-9">
                                        <select class="select2 form-control custom-select" name="carrier_id" style="width: 100%; height:36px;" required>
                                            <option value="">Select Courier</option>
                                            @foreach($carriers as $carrier)
                                                <option value="{{ $carrier->id }}" @if (isset($edit)) @if ($carrier->id == $rate->carrier_id) selected @endif @endif>{{ $carrier->title }}</option>
                                            @endforeach
                                        </select>
                                        @error ('carrier_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label for="type_id" class="col-sm-3 text-right control-label col-form-label">Type<span class="text-danger">*</span></label>
                                    <div class="controls col-sm-9">
                                        <select class="select2 form-control custom-select" name="type_id" style="width: 100%; height:36px;" required>
                                            <option value="">Select Type</option>
                                            @foreach($types as $type)
                                                <option value="{{ $type->id }}" @if (isset($edit)) @if ($type->id == $rate->type_id) selected @endif @endif>{{ $type->title }}</option>
                                            @endforeach
                                        </select>
                                        @error ('type_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label for="distance_from" class="col-sm-3 text-right control-label col-form-label">Distance from<span class="text-danger">*</span></label>
                                    <div class="controls col-sm-9">
                                        <input type="number" name="distance_from" min="0" value="@if(isset($edit)){{ $rate->distance_from }}@else{{ old('distance_from') }}@endif" class="form-control{{ $errors->has('distance_from') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                        @error ('distance_from')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label for="distance_to" class="col-sm-3 text-right control-label col-form-label">Distance to<span class="text-danger">*</span></label>
                                    <div class="controls col-sm-9">
                                        <input type="number" name="distance_to" min="0" value="@if(isset($edit)){{ $rate->distance_to }}@else{{ old('distance_to') }}@endif" class="form-control{{ $errors->has('distance_to') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                        @error ('distance_to')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label for="weight_from" class="col-sm-3 text-right control-label col-form-label">Weight from<span class="text-danger">*</span></label>
                                    <div class="controls col-sm-9">
                                        <input type="number" name="weight_from" min="0" value="@if(isset($edit)){{ $rate->weight_from }}@else{{ old('weight_from') }}@endif" class="form-control{{ $errors->has('weight_from') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                        @error ('distance_from')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label for="weight_to" class="col-sm-3 text-right control-label col-form-label">Weight to<span class="text-danger">*</span></label>
                                    <div class="controls col-sm-9">
                                        <input type="number" name="weight_to" min="0" value="@if(isset($edit)){{ $rate->weight_to }}@else{{ old('weight_to') }}@endif" class="form-control{{ $errors->has('weight_to') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                        @error ('weight_to')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label for="length_from" class="col-sm-3 text-right control-label col-form-label">Length from<span class="text-danger">*</span></label>
                                    <div class="controls col-sm-9">
                                        <input type="number" name="length_from" min="0" value="@if(isset($edit)){{ $rate->length_from }}@else{{ old('length_from') }}@endif" class="form-control{{ $errors->has('length_from') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                        @error ('length_from')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label for="length_to" class="col-sm-3 text-right control-label col-form-label">Length to<span class="text-danger">*</span></label>
                                    <div class="controls col-sm-9">
                                        <input type="number" name="length_to" min="0" value="@if(isset($edit)){{ $rate->length_to }}@else{{ old('length_to') }}@endif" class="form-control{{ $errors->has('length_to') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                        @error ('length_to')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label for="width_from" class="col-sm-3 text-right control-label col-form-label">Width from<span class="text-danger">*</span></label>
                                    <div class="controls col-sm-9">
                                        <input type="number" name="width_from" min="0" value="@if(isset($edit)){{ $rate->width_from }}@else{{ old('width_from') }}@endif" class="form-control{{ $errors->has('width_from') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                        @error ('width_from')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label for="width_to" class="col-sm-3 text-right control-label col-form-label">Width to<span class="text-danger">*</span></label>
                                    <div class="controls col-sm-9">
                                        <input type="number" name="width_to" min="0" value="@if(isset($edit)){{ $rate->width_to }}@else{{ old('width_to') }}@endif" class="form-control{{ $errors->has('width_to') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                        @error ('width_to')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label for="height_from" class="col-sm-3 text-right control-label col-form-label">Height from<span class="text-danger">*</span></label>
                                    <div class="controls col-sm-9">
                                        <input type="number" name="height_from" min="0" value="@if(isset($edit)){{ $rate->height_from }}@else{{ old('height_from') }}@endif" class="form-control{{ $errors->has('height_from') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                        @error ('height_from')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label for="height_to" class="col-sm-3 text-right control-label col-form-label">Height to<span class="text-danger">*</span></label>
                                    <div class="controls col-sm-9">
                                        <input type="number" name="height_to" min="0" value="@if(isset($edit)){{ $rate->height_to }}@else{{ old('height_to') }}@endif" class="form-control{{ $errors->has('height_to') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                        @error ('height_to')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 col-lg-6">
                                <div class="form-group row">
                                    <label for="price" class="col-sm-3 text-right control-label col-form-label">Price<span class="text-danger">*</span></label>
                                    <div class="controls col-sm-9">
                                        <input type="number" name="price" min="0" value="@if(isset($edit)){{ $rate->price }}@else{{ old('price') }}@endif" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                        @error ('price')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card-body">
                        <div class="form-group mb-0 text-right">
                            <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                            <button type="submit" class="btn btn-dark waves-effect waves-light">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Row -->
@endsection
