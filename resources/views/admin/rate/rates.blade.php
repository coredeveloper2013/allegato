@extends('admin.layouts.app')

@section('title')
    Rates
@endsection

@section('heading')
    Rates
@endsection

@section('breadcrumb')
    Rates
@endsection

@section('content')
    @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {!! html_entity_decode(Session::get('success')) !!}
        </div>
    @endif
    <!-- Row created callback -->
    <div class="row">
        <div class="col-12">
            <div class="material-card card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-10">
                            <h4 class="card-title">Rates</h4>
                        </div>
                        <div class="col-md-2 text-right">
                            <a href="{{ route('rates.create') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Add New</a>
                        </div>
                        <div class="col-12">
                            <hr>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-striped table-hover border display" style="width: 100%">
                            <thead>
                            <tr>
                                <th>Courier</th>
                                <th>Type</th>
                                <th>Distance range</th>
                                <th>Weight range</th>
                                <th>Length range</th>
                                <th>Width range</th>
                                <th>Height range</th>
                                <th>Price</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($rates as $rate)
                                <tr>
                                    <td>{{ $rate->carrier->title }}</td>
                                    <td>{{ $rate->type->title }}</td>
                                    <td>{{ $rate->distance_from }} - {{ $rate->distance_to }}</td>
                                    <td>{{ $rate->weight_from }} - {{ $rate->weight_to }}</td>
                                    <td>{{ $rate->length_from }} - {{ $rate->length_to }}</td>
                                    <td>{{ $rate->width_from }} - {{ $rate->width_to }}</td>
                                    <td>{{ $rate->height_from }} - {{ $rate->height_to }}</td>
                                    <td>€ {{ $rate->price }}</td>
                                    <td>
                                        <form action="{{ route('rates.destroy', $rate->id)}}" method="post">
                                            @csrf
                                            @method('DELETE')
                                            <a href="{{ route('rates.show', $rate->id)}}" class="btn btn-info btn-circle"><i class="fa fa-eye"></i> </a>
                                            <a href="{{ route('rates.edit', $rate->id)}}" class="btn btn-warning btn-circle"><i class="fa fa-edit"></i> </a>
                                            <button type="submit" onclick="return confirm('Are you sure want to delete this data?')" class="btn btn-danger btn-circle"><i class="fa fa-trash"></i> </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
