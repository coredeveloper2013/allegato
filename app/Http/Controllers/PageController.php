<?php

namespace App\Http\Controllers;

use App\Page;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin')->except('slug');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Get all resources
        $pages = Page::all()->sortByDesc('id');

        return view('admin.page.pages', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // Show create form
        return view('admin.page.page-create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Validate form data
        $request->validate([
            'title' => 'required|string|max:255|unique:pages',
            'slug' => 'required|string|max:255|unique:pages',
            'meta_title' => 'required|string|max:255',
            'meta_description' => 'required|string|max:255',
            'content' => 'required|string',
            'status' => 'required|boolean',
        ]);

        // Create a new model instance assign form-data then save to DB
        $page = new Page();
        $page->title = $request->title;
        $page->content = $request->content;
        $page->status = $request->status;
        $page->slug = $request->slug;
        $page->meta_title = $request->meta_title;
        $page->meta_description = $request->meta_description;
        $page->save();

        return redirect()->back()->with('success', 'Page created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        // Get the resource
        $page = Page::find($page->id);

        return view('admin.page.page-show', compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        // Get the model & show data to edit form
        $page = Page::find($page->id);
        $edit = true;

        return view('admin.page.page-create-edit', compact('page', 'edit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        // Validate form data
        $request->validate([
            'title' => 'required|string|max:255',
            'slug' => 'required|string|max:255',
            'meta_title' => 'required|string|max:255',
            'meta_description' => 'required|string|max:255',
            'content' => 'required|string',
            'status' => 'required|boolean',
        ]);

        // Get the model assign form-data then save to DB
        $page = Page::find($page->id);

        if ($request->title != $page->title) {
            // Validate form data
            $request->validate([
                'title' => 'unique:pages',
            ]);

            $page->title = $request->title;
        }

        if ($request->slug != $page->slug) {
            // Validate form data
            $request->validate([
                'slug' => 'unique:pages',
            ]);

            $page->slug = $request->slug;
        }

        $page->content = $request->content;
        $page->status = $request->status;
        $page->slug = $request->slug;
        $page->meta_title = $request->meta_title;
        $page->meta_description = $request->meta_description;
        $page->save();

        return redirect()->back()->with('success', 'Page updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        // Get the resource & delete it from DB
        Page::destroy($page->id);

        return redirect()->back()->with('success', 'Page deleted successfully.');
    }

    public function slug($slug)
    {
        $page = Page::where('slug', $slug)->first();

        if (!$page) {
            return abort( 404 );
        }

        return view('pages.page', compact('page'));
    }
}
