@extends('admin.layouts.app')

@section('title')
  Dashboard
@endsection

@section('heading')
    Dashboard
@endsection

    @section('breadcrumb')
      Dashboard
    @endsection

     @section('content')
         <!-- ============================================================== -->
         <!-- Card Group  -->
         <!-- ============================================================== -->
         <div class="card-group">
             <div class="card p-2 p-lg-3">
                 <div class="p-lg-3 p-2">
                     <div class="d-flex align-items-center">
                         <button class="btn btn-circle btn-danger text-white btn-lg" href="javascript:void(0)">
                             <i class="mdi mdi-account-multiple"></i>
                         </button>
                         <div class="ml-4" style="width: 38%;">
                             <h4 class="font-light">Total Clients</h4>
                             <div class="progress">
                                 <div class="progress-bar bg-danger" role="progressbar" style="width: {{ $total }}%" aria-valuenow="{{ $total }}" aria-valuemin="0" aria-valuemax="100"></div>
                             </div>
                         </div>
                         <div class="ml-auto">
                             <h2 class="display-7 mb-0">{{ $total }}</h2>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="card p-2 p-lg-3">
                 <div class="p-lg-3 p-2">
                     <div class="d-flex align-items-center">
                         <button class="btn btn-circle btn-cyan text-white btn-lg" href="javascript:void(0)">
                             <i class="fas fa-euro-sign"></i>
                         </button>
                         <div class="ml-4" style="width: 38%;">
                             <h4 class="font-light">Total Orders</h4>
                             <div class="progress">
                                 <div class="progress-bar bg-cyan" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                             </div>
                         </div>
                         <div class="ml-auto">
                             <h2 class="display-7 mb-0">76</h2>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="card p-2 p-lg-3">
                 <div class="p-lg-3 p-2">
                     <div class="d-flex align-items-center">
                         <button class="btn btn-circle btn-warning text-white btn-lg" href="javascript:void(0)">
                             <i class="mdi mdi-truck-delivery"></i>
                         </button>
                         <div class="ml-4" style="width: 38%;">
                             <h4 class="font-light">Total Delivered</h4>
                             <div class="progress">
                                 <div class="progress-bar bg-warning" role="progressbar" style="width: 40%" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
                             </div>
                         </div>
                         <div class="ml-auto">
                             <h2 class="display-7 mb-0">83</h2>
                         </div>
                     </div>
                 </div>
             </div>
         </div>
         <!-- ============================================================== -->
         <!-- User Table & Profile Cards Section  -->
         <!-- ============================================================== -->
         <div class="row">
             <div class="col-md-12">
                 <div class="card">
                     <div class="card-body">
                         <h5 class="card-title text-uppercase mb-0">Latest Registered Users</h5>
                     </div>
                     <div class="table-responsive">
                         <table class="table no-wrap user-table mb-0">
                             <thead>
                             <tr>
                                 <th scope="col" class="border-0 text-uppercase font-medium pl-4">#</th>
                                 <th scope="col" class="border-0 text-uppercase font-medium">Name</th>
                                 <th scope="col" class="border-0 text-uppercase font-medium">Address</th>
                                 <th scope="col" class="border-0 text-uppercase font-medium">Email</th>
                                 <th scope="col" class="border-0 text-uppercase font-medium">Phone</th>
                             </tr>
                             </thead>
                             <tbody>
                             @php $i = 0; @endphp
                             @foreach($clients as $client)
                                 @php $i ++; @endphp
                             <tr>
                                 <td class="pl-4">{{ $i }}</td>
                                 <td>
                                     <h5 class="font-medium mb-0">{{ $client->name }}</h5>
                                     <span class="text-muted">{{ $client->state }}, {{ $client->country }}</span>
                                 </td>
                                 <td>
                                     <span class="text-muted">{{ $client->address }}</span><br>
                                     <span class="text-muted">Zip-code : {{ $client->zipCode }}</span>
                                 </td>
                                 <td>
                                     <span class="text-muted">{{ $client->email }}</span>
                                 </td>
                                 <td>
                                     <span class="text-muted">{{ $client->phone }}</span>
                                 </td>
                             </tr>
                             @endforeach
                             </tbody>
                         </table>
                     </div>
                 </div>
             </div>
         </div>
      @endsection
