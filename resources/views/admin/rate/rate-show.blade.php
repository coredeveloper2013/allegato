@extends('admin.layouts.app')

@section('title')
    Rate show
@endsection

@section('heading')
    Rate show
@endsection

@section('breadcrumb')
    Rate show
@endsection

@section('content')
    <!-- Row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header bg-info">
                    <h4 class="mb-0 text-white">Form with view only</h4>
                </div>
                <form class="form-horizontal">
                    <div class="form-body">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-10">
                                    <h4 class="card-title">Rate show</h4>
                                </div>
                                <div class="col-md-2 text-right">
                                    <a href="{{ route('rates.index') }}" class="btn btn-info"><i class="mdi mdi-step-backward"></i> Back to list</a>
                                </div>
                            </div>
                        </div>
                        <hr class="mt-0 mb-5">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Courier:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $rate->carrier->title }} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Type:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $rate->type->title }} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Distance range:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $rate->distance_from }} - {{ $rate->distance_to }} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Weight range:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $rate->weight_from }} - {{ $rate->weight_to }} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Length range:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $rate->length_from }} - {{ $rate->length_to }} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Width range:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $rate->width_from }} - {{ $rate->width_to }} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Height range:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> {{ $rate->height_from }} - {{ $rate->height_to }} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="control-label text-right col-md-3">Price:</label>
                                        <div class="col-md-9">
                                        <p class="form-control-static">€ {{ $rate->price }} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                        </div>
                        <hr>
                        <div class="form-actions">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <a href="{{ route('rates.edit', $rate->id)}}" class="btn btn-danger"> <i class="fa fa-pencil"></i> Edit</a>
                                                <a href="{{ route('rates.index') }}" class="btn btn-dark">Cancel</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6"> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Row -->
@endsection
