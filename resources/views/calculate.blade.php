@extends('layouts.app')

@section('title')
    Calculate
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="calculation-container">
                @if(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Holy guacamole!</strong> You should check in on some of those fields below.
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                <div>
                    <h2><strong>Risultati della ricerca</strong></h2>
                    <h5>Da Como a Roma - Peso: 20kg</h5>
                </div>

                <div class="row">
                    <div class="col-md-9"></div>
                    <div class="col-md-3">
                        <p>Data per il ritiro</p>
                    </div>
                </div>

                <div class="date-container row">
                    <div class="col-md-6">
                        <h4><strong>Risultati della ricerca</strong></h4>
                    </div>
                    <div class="col-md-6">
                        <div class="float-right">
                            <input class="form-control check_in" type="text" name="start_date" required>
                        </div>
                    </div>
                </div>


                <form class="calculation-form needs-validation" action="{{ route('address.get') }}" method="get" novalidate>
                    <input type="hidden" name="" value="{{ $request }}">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="formGroupExampleInput">Example label</label>
                                    <input type="text" class="form-control" placeholder="First name" required>
                                    <div class="invalid-tooltip">
                                        This field is required.
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="formGroupExampleInput">&nbsp;</label>
                                    <input type="text" class="form-control" placeholder="Last name">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control" placeholder="First name">
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control" placeholder="Last name">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="formGroupExampleInput">Example label</label>
                                    <input type="text" class="form-control" placeholder="First name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleFormControlSelect1">Example select</label>
                                    <select class="form-control" id="exampleFormControlSelect1">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="comment">Comment:</label>
                                <textarea class="form-control" rows="7" id="comment"></textarea>
                                <small class="form-text text-muted">
                                    Your password must be 8-20 characters long, contain letters and numbers.
                                </small>
                            </div>
                        </div>
                    </div>

                    <hr style="border: 1px solid #EFEFEF; margin: 30px 0 40px">

                    <h4 class="text-muted">Risultati della ricerca</h4>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="formGroupExampleInput">Example label</label>
                                    <input type="text" class="form-control" placeholder="First name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="formGroupExampleInput">&nbsp;</label>
                                    <input type="text" class="form-control" placeholder="Last name">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control" placeholder="First name">
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control" placeholder="Last name">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="formGroupExampleInput">Example label</label>
                                    <input type="text" class="form-control" placeholder="First name">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="exampleFormControlSelect1">Example select</label>
                                    <select class="form-control" id="exampleFormControlSelect1">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="comment">Comment:</label>
                                <textarea class="form-control" rows="7" id="comment"></textarea>
                                <small class="form-text text-muted">
                                    Your password must be 8-20 characters long, contain letters and numbers.
                                </small>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="icon-container">
                                <button type="button" class="btn btn-success float-right"><i class="fa fa-clone"></i></button>
                                <button type="button" class="btn btn-success float-right"><i class="fa fa-times"></i></button>
                                <button type="button" class="btn btn-success float-right"><i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="float-right"><button type="submit" class="submit-button btn btn-success">Continue</button></div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script src="{{ asset('js/flatpickr.js') }}"></script>
<script>
    $(".check_in").flatpickr({
        defaultDate: 'today',
        minDate: 'today',
        altInput: true,
        altFormat: 'F j, Y'
    });
</script>
@endsection
